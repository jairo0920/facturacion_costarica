
function calculaLetraNIF()
    {
        var nif=$("[name='cifnif']").val();
        if(!isNaN(nif) && nif !=='')
        {
        if( confirm('¿Quieres calcular la letra del NIF?') )
        {
            $.ajax({
                url: location.href,
                async: false,
                data: {'action': 'letraNIF','valor': nif},
                type: 'POST',
                datatype: 'text',
                sucess: function(data){
                    $("[name='cifnif']").val('sucess' + nif + data['responseText']);
                },
                complete:function(data){
                    console.log(data);
                    if(data['readyState']===4){
                        $("[name='cifnif']").val( nif + data['responseText']);
                    }
                },
                error: function(xhr,status){
                    alert('Ha ocurrido algún tipo de error ' + status);
                }
            });
        }
    }
}
$(document).ready(function(){
    $("[name='cifnif']").on('change',calculaLetraNIF());
    $("[name='cifnif']").blur(function(){
    calculaLetraNIF();
    });
    
    });
