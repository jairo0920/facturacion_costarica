<?php
/**
 * This file is part of FacturaScripts
 * Copyright (C) 2017-2018 Carlos Garcia Gomez <carlos@facturascripts.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
namespace FacturaScripts\Plugins\FacturacionCostaRica\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

/**
  * Controller to edit a single item from the Empresa model
  *
  * @author Jairo Alberto Cruz Sibaja  <jairo@cruz.cr>
  *
  */
class ListFacturaElectronica extends ExtendedController\ListController
{

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['title'] = 'Factura Electronica';
        $pagedata['icon'] = 'fas fa-award';
        $pagedata['menu'] = 'admin';

        return $pagedata;
    }

    public function privateCore(&$response, $user, $permissions)
    {
        parent::privateCore($response, $user, $permissions);
        $this->setTemplate('FacturaElectronica');
    }


    protected function createViews()
    {
        $this->addView('ListEmpresa', 'Empresa', 'companies', 'fas fa-building');
        $this->addSearchFields('ListEmpresa', ['nombre', 'nombrecorto']);
        $this->addOrderBy('ListEmpresa', ['idempresa'], 'code');
        $this->addOrderBy('ListEmpresa', ['nombre'], 'name');
    }

}
