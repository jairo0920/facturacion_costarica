<?php
/**
  * This file is part of FacturaScripts
  * Copyright (C) 2013-2019 Carlos Garcia Gomez <carlos@facturascripts.com>
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU Lesser General Public License as
  * published by the Free Software Foundation, either version 3 of the
  * License, or (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
  */
namespace FacturaScripts\Plugins\FacturacionCostaRica\Model;

use FacturaScripts\Core\Model\Empresa as ParentModel;
use FacturaScripts\Core\App\AppSettings;
use FacturaScripts\Core\Model\Base;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
  * Controller to edit a single item from the Empresa model
  *
  * @author Jairo Alberto Cruz Sibaja  <jairo@cruz.cr>
  *
  */
class Empresa extends ParentModel
{

    use Base\ModelTrait;
    /**
     * Website of the person.
     *
     * @var string
     */
    public $regimenMH;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $tipo_indetif_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $idProvincia_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $idCanton_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $idDistrito_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $idBarrio_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $cod_pais_tel_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $cod_cod_pais_fax_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $ENV_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $situacion_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $stagP12Url_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $prodP12Url_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $stagUserName_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $stagPassword_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $prodUserName_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $prodPassword_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $prodP12Code_fe;

    /**
     * Website of the person.
     *
     * @var string
     */
    public $stagP12Code_fe;

    /**
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->idempresa == AppSettings::get('default', 'idempresa')) {
            self::$miniLog->alert('you-cant-not-remove-default-company');
            return false;
        }

        return parent::delete();
    }

    /**
     * This function is called when creating the model table. Returns the SQL
     * that will be executed after the creation of the table. Useful to insert values
     * default.
     *
     * @return string
     */
    public function install()
    {
        $num = mt_rand(1, 9999);

        return 'INSERT INTO ' . static::tableName() . ' (idempresa,web,codpais,'
            . 'direccion,administrador,cifnif,nombre,nombrecorto,personafisica,regimeniva)'
            . "VALUES (1,'https://www.facturascripts.com','ESP','C/ Falsa, 123',"
            . "'','00000014Z','Empresa " . $num . " S.L.','E-" . $num . "','0',"
            . "'" . self::$regimenIVA->defaultValue() . "');";
    }

    /**
     * Returns the name of the column that is the model's primary key.
     *
     * @return string
     */
    public static function primaryColumn()
    {
        return 'idempresa';
    }

    /**
     * Returns the description of the column that is the model's primary key.
     *
     * @return string
     */
    public function primaryDescriptionColumn()
    {
        return 'nombrecorto';
    }

    /**
     * Returns the name of the table that uses this model.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * Check the company's data, return TRUE if correct
     *
     * @return bool
     */
    public function test()
    {
        $this->administrador = Utils::noHtml($this->administrador);
        $this->apartado = Utils::noHtml($this->apartado);
        $this->ciudad = Utils::noHtml($this->ciudad);
        $this->codpostal = Utils::noHtml($this->codpostal);
        $this->direccion = Utils::noHtml($this->direccion);
        $this->nombrecorto = Utils::noHtml($this->nombrecorto);
        $this->provincia = Utils::noHtml($this->provincia);
        $this->web = Utils::noHtml($this->web);
        $this->regimenMH = Utils::noHtml($this->regimenMH);
        $this->tipo_indetif_fe = Utils::noHtml($this->tipo_indetif_fe);
        $this->idProvincia_fe = Utils::noHtml($this->idProvincia_fe);
        $this->idCanton_fe = Utils::noHtml($this->idCanton_fe);
        $this->idDistrito_fe = Utils::noHtml($this->idDistrito_fe);
        $this->idBarrio_fe = Utils::noHtml($this->idBarrio_fe);
        $this->cod_pais_tel_fe = Utils::noHtml($this->cod_pais_tel_fe);
        $this->cod_cod_pais_fax_fe = Utils::noHtml($this->cod_cod_pais_fax_fe);
        $this->ENV_fe = Utils::noHtml($this->ENV_fe);
        $this->situacion_fe = Utils::noHtml($this->situacion_fe);
        $this->stagP12Url_fe = Utils::noHtml($this->stagP12Url_fe);
        $this->prodP12Url_fe = Utils::noHtml($this->prodP12Url_fe);
        $this->stagUserName_fe = Utils::noHtml($this->stagUserName_fe);
        $this->stagPassword_fe = Utils::noHtml($this->stagPassword_fe);
        $this->prodUserName_fe = Utils::noHtml($this->prodUserName_fe);
        $this->prodPassword_fe = Utils::noHtml($this->prodPassword_fe);
        $this->prodP12Code_fe = Utils::noHtml($this->prodP12Code_fe);
        $this->stagP12Code_fe = Utils::noHtml($this->web);

        if (empty($this->idempresa)) {
            $this->idempresa = $this->newCode();
        }

        return parent::test();
    }
}
